json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :adult, :children, :check_in, :check_out, :procedence, :motive
  json.url reservation_url(reservation, format: :json)
end
