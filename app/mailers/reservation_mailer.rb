class ReservationMailer < ActionMailer::Base
  default from: "reservas@tierradeavesposada.com"
 
  def reservation(reservation)
  	@reservation = reservation
    mail(to: "reservas@tierradeavesposada.com", subject: reservation.client.name+' ha solicitado una reservación.')
  end

  def client(reservation)
  	@reservation = reservation
    mail(to: reservation.email, subject: reservation.client.name+', su solicitud a sido recibida.')
  end
end