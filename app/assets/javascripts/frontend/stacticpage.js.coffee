$(document).on 'ready page:load', ->

  removeHash = ->
    history.pushState '', document.title, window.location.pathname + window.location.search
    return

  $('.datepicker').datepicker
    format: 'dd-mm-yyyy'
    autoclose: true
    language: 'es'
  $('.top-nav').onePageNav
    currentClass: 'current'
    changeHash: false
    scrollSpeed: 500
    scrollThreshold: 0.5
    filter: ':not(.external)'
    easing: 'swing'
    begin: ->   

    end: ->  

  if $('#home').text() != ''
    num = parseInt($('#home').css('height').split('px')[0])-10

    $(window).bind 'scroll', ->
      if $(window).scrollTop() > num
        $('#front-navbar').addClass 'navbar-fixed-top'
      else
        $('#front-navbar').removeClass 'navbar-fixed-top'
      return
  return
