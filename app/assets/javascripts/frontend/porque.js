$(document).on('ready page:load', function(arguments){
  var i = 1;
  var text1 = '<li id="text-1">CONECTA CON LA NATURALEZA</li>'
  var text2 = '<li id="text-2">UN ESPACIO ÚNICO PARA EL DESCANSO</li>'
  var text3 = '<li id="text-3">CONOCE QUISIRO Y SUS ATRACTIVOS ÚNICOS</li>'
  var array = [text1, text2, text3]

  $(function scrollable () {
    $("#text-"+i).animate({
      "margin-top":"-=20",
    }, 100, function() {
      $("#text-"+i).remove();
      $(".scrollable").append(array[i-1])
      i++;
      if(i>3) i=1;
    });
    setTimeout(scrollable, 2000);
  });


  function timedCount(i) {
    if (i > 3) { i = 1; }
    actual = i;
    if (i == 3) { next = 1 } else { next = i+1; }
    $("#div-"+actual).removeClass('active');
    $("#div-"+next).addClass('active');
    setTimeout(function(){ i++; timedCount(i); }, 10000);
  }

  timedCount(1);
});