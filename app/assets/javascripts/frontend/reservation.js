function quantity(id){
	$(document).ready(function(){ 
		if($("#id-"+id).hasClass('active')==false){
			$('#reservation_assignments_attributes_'+(id-1)+'_quantity').prop('min', '0');
			$('#reservation_assignments_attributes_'+(id-1)+'_room_id_'+id).prop('checked', true);
			$("#id-"+id).addClass( "active" );
			$("#mask-"+id).addClass( "green" );
			$("#id-"+id+" input").animate({
				opacity: 1,
				top: '23px'
			});
		}else{
			$("#id-"+id).removeClass( "active" );
			$("#mask-"+id).removeClass( "green" );
			$('#reservation_assignments_attributes_'+(id-1)+'_room_id_'+id).prop('checked', false);
			$("#id-"+id+" input").animate({
				opacity: 0,
				top: '-10px'
			}, 500, function(){
				$("#id-"+id+" input").val("");
			});
		} 
	});
}

$(document).on('ready page:load', function(arguments){	
	jQuery(function() {
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		var checkin = $('#reservation_check_in').datepicker({
			format: "dd-mm-yyyy",
			autoclose: true,
	   		todayHighlight: true,
		    beforeShowDay: function (date) {
		      return date.valueOf() >= now.valueOf();
		    },
		    autoclose: true
		}).on('changeDate', function (ev) {		    
		  $('#reservation_check_out')[0].focus();
		});
		var checkout = $('#reservation_check_out').datepicker({
			format: "dd-mm-yyyy",
			autoclose: true,
	    	todayHighlight: true,
	    	beforeShowDay: function (date) {
				return date.valueOf() >= now.valueOf();
			},
			  autoclose: true,
			}).on('changeDate', function (ev) {
					
		});
	});
});
