$(document).ready ->
	home =
		windowHeight: (sltr)->
			sltr.css "height", $(window).height()

	vector = [
		$("#home"), 
		$(".responsive-container"), 
		$(".img-container"), 
		$(".dummy")
	]

	$.each vector, (id, selector)->
		home.windowHeight(selector)

	$(window).resize ->	  
		$.each vector, (id, selector)->
			home.windowHeight(selector)