require 'elasticsearch/model'
class Reservation < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :assignments_filter
  before_destroy :assignments_destroy
  #validaciones#####################################
  validates_presence_of :adult, :check_in, :check_out, :pay, :assignments
  validate :valid_date_range_required
  validate :valid_check_in
  validate :validates_rooms
  validates_numericality_of :phone, :adult
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  #Relaciones#######################################
  belongs_to :client
  belongs_to :pay
  has_many :assignments
  has_many :rooms, through: :assignments
  accepts_nested_attributes_for :client
  accepts_nested_attributes_for :assignments
  
	def as_json(options = {})
		{
			id: self.id,
			cedula: self.client.cedula,
			name: self.client.name,
			email: self.email,
			phone: self.phone,
			adult: self.adult,
			children: self.children,
			check_in: self.check_in,
			check_out: self.check_out,
			motive: self.motive,
			pay: pay_name(self.pay_id),
			rooms: room_names(self.assignments),
			created_at: self.created_at,
			status: status_reservation(self.check_in, self.check_out)
		}
	end

	def self.query(query)
    { query: { multi_match:  { query: query, fields: [:cedula, :name, :email, :check_in, :check_out, :pay] , operator: :and }  }, sort: { id: "desc" }, size: Reservation.count }
  end

  #params[:search] is false
  def self.index(current_user)
    Reservation.all.order("created_at DESC")
  end
  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.where("id != #{current_user.id}").order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.where("id != #{current_user.id}").count
  end

  def pay_name(pay)
		if pay == 0 or pay.nil?
			return "Sin metodo de pago"
		else
			return self.pay.name
		end
	end

	def valid_date_range_required
		unless self.check_in.blank? or self.check_out.blank?
			errors.add(:check_out, "El check-out no puede ser despues del check-in") if Date.parse(self.check_in) > Date.parse(self.check_out)
		end
	end

	def valid_check_in
		unless self.check_in.blank? or self.check_out.blank?
			errors.add(:check_in, "El check-in que usted ingresó no es valido") if Date.parse(self.check_in) < Date.parse(Time.new.to_s)
			errors.add(:check_out, "El check-in que usted ingresó no es valido") if Date.parse(self.check_out) < Date.parse(Time.new.to_s)
		end
	end

	def validates_rooms
		room_hash = []
		self.assignments.each do |assignment|
			unless assignment.quantity.blank?
				room_hash << assignment.quantity
			end
		end
		if room_hash.blank?
			errors.add(:assignments, "Debe elegir una habitación")
		end
	end

	def room_names(assignments)
		assignment_hash = []
		self.assignments.each do |assignment|
			room = Room.find(assignment.room_id)
			assignment_hash << ["#{room.name}", assignment.quantity]
		end
		return assignment_hash.to_a
	end

	def assignments_filter
		self.assignments.each do |assignment|
			if assignment.quantity.blank?
				self.assignments.delete(assignment)
			end			
		end
	end

	def assignments_destroy
		assignments = Assignment.where(reservation_id: self.id)
		assignments.each do |assignment|
			assignment.delete
		end
	end

	def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def status_reservation(check_in, check_out)	
  	check_in = Date.parse(check_in)
  	check_out = Date.parse(check_out)
  	time_now = Date.parse(Time.new.to_s)

		if  time_now < check_in
			return 1
		elsif time_now >= check_in and time_now <= check_out
			return 2
		else
			return 3
		end
	end

  def self.multiple_destroy(ids, current_user)
    Reservation.destroy ids
  end
end

Reservation.import
