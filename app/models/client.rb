class Client < ActiveRecord::Base
	has_many :reservations, dependent: :destroy	
	validates_presence_of :cedula, :name
end
