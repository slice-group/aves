class Room < ActiveRecord::Base
	has_many :assignments
  	has_many :reservations, through: :assignments
end
