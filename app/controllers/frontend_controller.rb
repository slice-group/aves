class FrontendController < ApplicationController
	layout 'layouts/frontend/application'  
  def index
  	Location.add(request.remote_ip) 
  	@items = items_hash 
    @rooms = InyxCatalogRails::Catalog.where(public: true, category: "Habitaciones")	
  end

  def show_room
  	Location.add(request.remote_ip) 
  	@room = InyxCatalogRails::Catalog.where(id: params[:id], category: "Habitaciones").first
  end

  def show_place
    Location.add(request.remote_ip) 
    @place = InyxCatalogRails::Catalog.where(id: params[:id], category: "Lugares").first
  end

  private
  	def items_hash
  		items = [  		
	  		["PASEOS TURÍSTICOS", "/assets/frontend/paseos.png", "6"], 
	  		["SUITES", "/assets/frontend/suite.png", "1", "h"], 		
	  		["SENDEROS DE INTERPRETACIÓN", "/assets/frontend/sendero.png", "7"],
	  		["HABITACIONES SENCILLAS", "/assets/frontend/sencilla.png", "2", "h"], 
	  		["HABITACIONES DOBLES", "/assets/frontend/doble.png", "3", "h"],  		
	  		["HABITACIONES FAMILIARES", "/assets/frontend/familiar.png", "4", "h"],
	  		["HABITACIONES MULTIPLES", "/assets/frontend/multiple.png", "5", "h"],
	  	]	
	  	return items 
  	end
end
