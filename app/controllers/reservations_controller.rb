class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:show, :edit, :update, :destroy]
  layout :layout_by_resource
  load_and_authorize_resource
  respond_to :html
  skip_before_filter :verify_authenticity_token  

  def show
    Location.add(request.remote_ip) 
    respond_with(@reservation)
  end

  def new
    Location.add(request.remote_ip) 
    @reservation = Reservation.new
    @reservation.client = Client.new
    @check_in = params[:check_in]
    @check_out = params[:check_out]
    @rooms_type = ["suite", "sencilla", "doble", "familiar", "multiple"]
    respond_with(@reservation)    
  end

  def edit
    Location.add(request.remote_ip) 
  end

  def create
    Location.add(request.remote_ip) 
    puts "Habitaciones #{reservation_params[:assignments_attributes]}"
    @client = Client.find_by_cedula(reservation_params[:client_attributes][:cedula])
    if !@client
      @reservation = Reservation.new(reservation_params)
      if @reservation.save
        ReservationMailer.reservation(@reservation).deliver
        ReservationMailer.client(@reservation).deliver
        flash[:notice] = "Su solicitud de reservación fue realizada exitosamente"
        redirect_to new_reservation_path
      else
        respond_with(@reservation)
      end
    else
      @reservation = Reservation.new(reservation_params)
      @reservation.client = @client
      if @reservation.save
        ReservationMailer.reservation(@reservation).deliver
        ReservationMailer.client(@reservation).deliver
        flash[:notice] = "Su solicitud de reservación fue realizada exitosamente"
        redirect_to new_reservation_path
      else
        respond_with(@reservation)
      end
    end
  end

  def update
    Location.add(request.remote_ip) 
    @reservation.update(reservation_params)
    respond_with(@reservation)
  end

  def destroy
    Location.add(request.remote_ip) 
    @reservation.destroy
    redirect_to index_reservations_path
  end

  private

    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    def reservation_params
      params.require(:reservation).permit(:email, :phone, :adult, :children, :check_in, :check_out, :procedence, :motive, :pay_id, assignments_attributes:[:room_id, :reservation_id, :quantity], client_attributes: [:name, :cedula])
    end

    def layout_by_resource
      if action_name == "new" or action_name == "create"
        "frontend/application"
      else
        "admin/application"
      end
    end

end
