class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.belongs_to :reservation
      t.belongs_to :room
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
