class CreatePays < ActiveRecord::Migration
  def change
    create_table :pays do |t|
      t.string :name

      t.timestamps
    end
  end
end
