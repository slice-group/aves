class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|      
      t.string :email
      t.string :phone
      t.string :adult
      t.string :children
      t.string :check_in
      t.string :check_out
      t.string :procedence
      t.text :motive
      t.belongs_to :client, default: 0
      t.belongs_to :pay, default: 0

      t.timestamps
    end
  end
end
