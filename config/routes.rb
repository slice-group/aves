Rails.application.routes.draw do  

  root to: 'frontend#index'

  resources :reservations, only:[:new, :create]

  get "habitacion/:id", to: "frontend#show_room", as: "show_room"
  
  #get "lugar/:id", to: "frontend#show_place", as: "show_place"

  mount InyxCatalogRails::Engine, :at => '', as: 'catalog'

  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  scope :admin, except: [:new, :create] do
    resources :reservations do
      collection do
        post '/delete', to: 'reservations#delete'
        get '/', to: 'reservations#index', as: 'index'
      end
    end
  end
  scope :admin do
  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
