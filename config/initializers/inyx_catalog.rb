InyxCatalogRails.setup do |config|
	config.catalog_index = false
	config.attachment_index = false
	config.attachment_show = false
end